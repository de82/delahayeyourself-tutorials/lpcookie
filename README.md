# 📣 Disclaimer

> Le repository git que vous êtes en train de visualiser n'a eu qu'une vocation pédagogique pour accompagner les modules enseignés en DUT 1A, 2A, LPRGI et 1A de cycle ingénieur.

> Son propos n'avait qu'une portée pédagogique et peut ne pas refléter l'état actuel ou encore les bonnes pratiques de la/les technologie(s) utilisée. 

## 🎯 Utilisation

Vous êtes libre de réutiliser librement le code présent dans ce repository. Prenez garde que le code est ici daté, non mis à jour et potentiellement ouvert aux failles et/ou bugs.

## 🦕 Crédit

[Samy Delahaye](https://delahayeyourself.info)


## 🪴 Descriptif du projet


### ![](static/img/cookie.png) Cookie

Un simple site web pour découvrir Python, Flask, peewee, WTForms et bien d'autres choses ...

## How to ?

1. `git clone`
2. `pipenv --python 3`
3. `pipenv install`
4. `flask run`
5. Et go sur [127.0.0.1:5000](http://127.0.0.1:5000)

## Tests ?

La couverture est minimal mais suffisante pour découvrir les T.U et d'intégrations avec PyTest. 

Pour les lancer un simple: `pytest tests/ --disable-warnings`

## Purpose

This project is only for educational purpose. It's actually a basic application for generating a dummy website.

## Authors

> [Samy Delahaye](https://delahayeyourself.info)
